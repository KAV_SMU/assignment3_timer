//
//  ViewController.swift
//  Timer
//
//  Created by Dongxuan Ji on 9/16/19.
//  Copyright © 2019 Dongxuan Ji. All rights reserved.
//

import UIKit
import CoreData

fileprivate let cRecordH: CGFloat = 40

class ViewController: UIViewController, UITableViewDataSource, NSFetchedResultsControllerDelegate{
    
    

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var LeftButton: UIButton!
    @IBOutlet weak var RightButton: UIButton!

    @IBOutlet weak var DescriptionText: UITextField!
    
    @IBOutlet weak var RecordTable: UITableView!
    
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    //data var
    var time  = 0
    var timer = Timer()
    var isStart = false
    var cButtonH : CGFloat!
    var recordCount = 0
    
     var recArray = [NSManagedObject]()    //阵列
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
        RecordTable.dataSource = self
        RecordTable.delegate = self as? UITableViewDelegate
        setupUI()
 
 
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
 
    func initializeFetchedResultsController(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TimeRecords")
        let runTimeSort = NSSortDescriptor(key: "runTime", ascending: true)
        let decReSort = NSSortDescriptor(key: "decRe", ascending: true)
        request.sortDescriptors = [runTimeSort, decReSort]
        
        let moc = dataController.managedObjectContext
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    
    
    
    
    
  override func viewWillAppear(_ animated: Bool) {
        //设定资料库，将资料库里的记录取出放入recArray
        
        let AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
         let managedObectContext = AppDelegate.persistentContainer.viewContext
        
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TimeRecords")
        
        do {
            let result = try managedObectContext.fetch(fetchRequest)
            recArray = result as! [NSManagedObject]
            
        } catch{
            print("\(error)")
        }
    RecordTable.reloadData()    //refresh tableview
    }
    @IBAction func Rightbtn(_ sender: Any){
        if !isStart {
            isStart = true
            self.setBtnState(LeftButton, isEnabled: true)
            self.LeftButton.setTitle("Record", for: .normal)
            self.RightButton.setTitle("Stop", for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ViewController.updateTimer), userInfo: nil, repeats: true)
        }
        else{
            isStart = false
            
            self.LeftButton.setTitle("Reset", for: .normal)
            self.RightButton.setTitle("Continue", for: .normal)
            self.timer.invalidate()
        }
        
    }
    @IBAction func Leftbtn(_ sender: Any){
        
        
        if !isStart{
            time = 0
            recordCount = 0
            updateTimer()
            
            self.setBtnState(LeftButton, isEnabled: false)
            self.LeftButton.setTitle("Record", for: .normal)
            self.RightButton.setTitle("Start", for: .normal)
           
           
        }
        else {
           
        
        //    save()   //savt time in tableview
      //that's last assignment code
       /*     let recordLabel = UILabel()
            recordLabel.frame = CGRect(x: 40, y: self.view.frame.height/2 + CGFloat(recordCount)*cRecordH, width: self.view.frame.width-80, height: cRecordH)
            
            recordLabel.text = String(format: "record: %d %@ ", recArray, self.timeLabel.text!)
            recordLabel.font = UIFont.systemFont(ofSize: 20)
            recordLabel.textColor = UIColor.black
            self.view.addSubview(recordLabel)
            recordArray.append(recordLabel)
          
            NSLog("Record %d", recordCount)
 */
              self.view.addSubview(RecordTable)
             
           
        }
        
}
    private func save(decRe:String,runTime:Date){
              
    let moc = UIApplication.shared.delegate as! AppDelegate
    
        let managedObectContext = moc.persistentContainer.viewContext
        
    let entity = NSEntityDescription.entity(forEntityName: "TimeRecords", in: managedObectContext)
    
    //建立新纪录(creat new records)
        
    let newTimeRecords = NSManagedObject(entity: entity!, insertInto: managedObectContext)
    
        newTimeRecords.setValue(timeLabel.text, forKey: "runTime")
        
        
        
        newTimeRecords.setValue(DescriptionText.text, forKey: "decRe")
    
        //存档(save)
        
        do {
            try managedObectContext.save()
        } catch {
            print("\(error)")
        }
        
 
       
           }
   /*@IBAction func Reset(_ sender: Any){
        timer.invalidate()
        time = 0
        timeLabel.text = "00:00:00"
    }*/

    
    @objc func updateTimer() {
        
        
        time += 1
        let timeStr = String(format: "%02d:%02d:%02d", time/360000,time/6000, (time/100)%60)
                timeLabel.text = String(timeStr)
        
            
        }
    
    func setBtnState(_ button:UIButton, isEnabled: Bool) {
       
        if isEnabled {
            button.isEnabled = true
            button.alpha = 1.0
        }
        else {
            button.isEnabled = false
            button.alpha = 0.5
        }

    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recArray.count    //How manys records I have
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = recArray[indexPath.row].value(forKey: "decRe") as? String
        
       
        return cell
    }
    
}



extension ViewController {
    func setupUI() {
        isStart = false
        recordCount = 0
        time = 0
        self.updateTimer()
        self.setBtnState(LeftButton, isEnabled: false)
        self.LeftButton.setTitle("Record", for: .normal)
        self.RightButton.setTitle("Start", for: .normal)
        
       
        
    }
}


/*作业：添加Label*/

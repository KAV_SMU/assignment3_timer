//
//  TimeRecords+CoreDataProperties.swift
//  Timer
//
//  Created by Dongxuan Ji on 10/1/19.
//  Copyright © 2019 Dongxuan Ji. All rights reserved.
//
//

import Foundation
import CoreData


extension TimeRecords {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TimeRecords> {
        return NSFetchRequest<TimeRecords>(entityName: "TimeRecords")
    }

    @NSManaged public var runTime: Int64
    @NSManaged public var decRe: String?
    @NSManaged public var startTime: Date?

}
